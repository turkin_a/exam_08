import React, {Component} from 'react';
import './QuoteBox.css';
import '../../fonts/font-awesome.css';
import {NavLink} from "react-router-dom";

class QuoteBox extends Component {
  render() {
    return (
      <div className="QuoteBox">
        <div className="QuoteControl">
          <NavLink to={`/edit-quote/${this.props.quote.id}`} className="QuoteEdit">
            <i className="fa fa-edit" title="Edit quote"> </i>
          </NavLink>
          <span className="QuoteDelete" onClick={(id) => this.props.remove(this.props.quote.id)}>
            <i className="fa fa-times" title="Delete quote"> </i>
          </span>
        </div>
        <span className="QuoteText">{this.props.quote.text}</span>
        <span className="QuoteAuthor">-----<br />{this.props.quote.author}</span>
      </div>
    );
  }
}

export default QuoteBox;