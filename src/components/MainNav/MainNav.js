import React from 'react';
import './MainNav.css';
import {NavLink} from "react-router-dom";

const MainNav = props => {
  return (
    <ul className="MainNav">
      <li><NavLink to="/" exact className="Nav-Item" activeClassName="Selected">All</NavLink></li>
      {props.categories.map((cat) => (
        <li key={`nav-${cat.path}`}>
          <NavLink to={`/${cat.path}`} className="Nav-Item" activeClassName="Selected">{cat.title}</NavLink>
        </li>
      ))}
    </ul>
  );
};

export default MainNav;