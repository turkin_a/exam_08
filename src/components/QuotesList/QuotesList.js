import React, {Component} from 'react';
import './QuotesList.css';
import QuoteBox from "../QuoteBox/QuoteBox";

class QuotesList extends Component {
  render() {
    return (
      <div className="QuotesList">
        {this.props.quotes.map(quote => (
          <QuoteBox key={quote.id} quote={quote} remove={this.props.remove} />
        ))}
      </div>
    );
  }
}

export default QuotesList;