import React, {Component} from 'react';
import './NewQuote.css';
import Button from "../UI/Button/Button";

import axios from '../../axios-orders';

class NewQuote extends Component {
  state = {
    categories: [
      {title: 'Star Wars', path: 'star-wars'},
      {title: 'Famous people', path: 'famous-people'},
      {title: 'Saying', path: 'saying'},
      {title: 'Humour', path: 'humour'},
      {title: 'Motivational', path: 'motivational'}
    ],
    title: 'Star Wars',
    quote: {
      category: 'star-wars',
      author: '',
      text: ''
    }
  };

  handleChangeFormField = event => {
    const field = event.target.name;
    const value = event.target.value;
    const quote = {...this.state.quote};
    let title = this.state.title;
    quote[field] = value;

    if (field === 'category') {
      const index = this.state.categories.findIndex(cat => cat.title === value);
      quote.category = this.state.categories[index].path;
      title = this.state.categories[index].title;
    }

    this.setState({quote, title});
  };

  addQuote = (event) => {
    event.preventDefault();
    if (this.props.match.params.id) {
      axios.put(`quotes/${this.props.match.params.id}.json`, this.state.quote)
        .finally(() => this.props.history.goBack());
    } else {
      axios.post('quotes.json', this.state.quote)
        .finally(() => this.props.history.goBack());
    }
  };

  abortChanges = (event) => {
    event.preventDefault();
    this.props.history.goBack();
  };

  componentDidMount() {
    if (this.props.match.params.id) {
      axios.get(`quotes/${this.props.match.params.id}.json`).then(response => {
        const index = this.state.categories.findIndex(cat => cat.path === response.data.category);
        const title = this.state.categories[index].title;

        this.setState({quote: response.data, title});
      });
    }
  };

  render() {
    return (
      <div className="NewQuote">
        <h3 className="PageTitle">Submit new quote</h3>
        <form action="" className="NewQuoteForm">
          <p>Category:</p>
          <select name="category" className="FormField"
                  value={this.state.title} onChange={this.handleChangeFormField}>
            {this.state.categories.map(cat => (
              <option key={cat.path}>{cat.title}</option>
            ))}
          </select>
          <p>Author:</p>
          <input name="author" type="text" className="FormField"
                 value={this.state.quote.author} onChange={this.handleChangeFormField} />
          <p>Quote text:</p>
          <textarea name="text" className="FormField TextArea"
                    value={this.state.quote.text} onChange={this.handleChangeFormField} />
          <Button className={'FormBtn'} clicked={this.addQuote}>Save</Button>
          <Button className={'FormBtn'} clicked={this.abortChanges}>Cancel</Button>
        </form>
      </div>
    );
  }
}

export default NewQuote;