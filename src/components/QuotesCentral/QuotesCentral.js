import React, {Component} from 'react';
import './QuotesCentral.css';
import {NavLink} from "react-router-dom";

class QuotesCentral extends Component {
  render() {
    return (
      <div className="QuotesCentral">
        <h2 className="Logo">Quotes Central</h2>
        <ul className="ControlMenu">
          <li><NavLink to="/" exact className="Nav-Item" activeClassName="Selected">Quotes</NavLink></li>
          <li><NavLink to="/new-quote" exact className="Nav-Item" activeClassName="Selected">Submit new quote</NavLink></li>
        </ul>
      </div>
    );
  }
}

export default QuotesCentral;