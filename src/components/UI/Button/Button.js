import React from 'react';
import './Button.css';

const Button = props => {
  return (
    <button
      className={`Btn ${props.className ? props.className : ''}`}
      onClick={(event) => props.clicked(event)}
    >
      {props.children}
    </button>
  );
};

export default Button;