import React, {Component} from 'react';
import './Quotes.css';

import axios from "../../axios-orders";

import MainNav from "../../components/MainNav/MainNav";
import QuotesList from "../../components/QuotesList/QuotesList";

class Quotes extends Component {
  state = {
    categories: [
      {title: 'Star Wars', path: 'star-wars'},
      {title: 'Famous people', path: 'famous-people'},
      {title: 'Saying', path: 'saying'},
      {title: 'Humour', path: 'humour'},
      {title: 'Motivational', path: 'motivational'}
    ],
    quotes: [],
    currentCategory: null
  };

  removeQuote = (id) => {
    axios.delete(`quotes/${id}.json`).finally(this.getQuotes);
  };

  getQuotes = (category) => {
    let url = '';
    if (category) url = `?orderBy="category"&equalTo="${category}"`;
    axios.get('quotes.json' + url).then(response => {
      let quotes = [];

      for (let key in response.data) {
        let quote = response.data[key];
        quote.id = key;
        quotes.push(quote);
      }

      this.setState({quotes, currentCategory: this.props.match.params.category});
    });
  };

  componentDidMount() {
    this.getQuotes();
  }

  componentDidUpdate() {
    if (this.state.currentCategory !== this.props.match.params.category) {
      this.getQuotes(this.props.match.params.category);
    }
  }

  render() {
    return (
      <div className="Quotes">
        <MainNav categories={this.state.categories} />
        <QuotesList quotes={this.state.quotes} remove={this.removeQuote}/>
      </div>
    );
  }
}

export default Quotes;