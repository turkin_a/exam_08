import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";

import QuotesCentral from "./components/QuotesCentral/QuotesCentral";
import Quotes from "./containers/Quotes/Quotes";
import NewQuote from "./components/NewQuote/NewQuote";

class App extends Component {
  render() {
    return (
      <div className="App">
        <QuotesCentral />
        <Switch>
          <Route path="/" exact component={Quotes} />
          <Route path="/new-quote" component={NewQuote} />
          <Route path="/edit-quote/:id" component={NewQuote} />
          <Route path="/:category" exact component={Quotes} />
          <Route render={() => <h1>Page not found</h1>} />
        </Switch>
      </div>
    );
  }
}

export default App;
